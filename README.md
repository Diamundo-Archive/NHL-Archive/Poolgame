# Poolgame

**Poolgame!**  
_By Diamundo, (c) 2018_

## Controls
Press A/D to rotate the stick clockwise/counterclockwise.  
Use Ctrl + A/D to speed it up, and Shift + A/D to speed it up more.  
Use Shift + Ctrl + A/D to move the cue a quarter of circle.  

Press W/S to decrease/increase the shooting strength, and press Space to shoot. 

Use the left-mouse button to press and drag to rotate the table.  
Scroll to zoom in and out. Use the right-mouse button to press and drag to rotate the table before shooting, and to move the table laterally when any balls are roling.

## Rules

The goal is to pocket all of your balls, and then pocket the 8-ball.  
If you pocket a ball that doesn't belong to you, then boo.  
If you pocket the 8-ball, the game is over automatically and someone wins.  
By default, the first player shall pocket the striped balls (#'s 9 - 15), and the second player will pocket the solid-coloured balls (#'s 1 - 7).

## Debug

In the developer console, you can control which messages you get to see based on a variable: `debugLevel (int)`.  
Only messages with a level equal or lower to the specified `debugLevel` will be printed to the console.  
These are the possible values and messages:


| Level | Messages | 
| :--- | :--- |
| 0 | Nothing. |
| 1 | Events |
| 2 | **_(default)_** Balls that are hitting a hole |
| 3 | Balls stopping or hitting walls |
| 4 | Stick's shooting strength when firing |
| 5 | Information about all collisions |
| 6 | Information about all collision checks |
| 7 | Any and all keypresses |
| 8 | Other ball stuff. |
