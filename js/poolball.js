class PoolBall {
	
	constructor ( /* int */ num ) {
		this.number = num;
		this.speed = new THREE.Vector2(0.0, 0.0);
	
		var ballRadius = 2.86;
		var sphere = new THREE.SphereGeometry(ballRadius, 64, 64); //ballRadius * 2 = 5.72, official ball size
		
		var colours	= {
			'0' : "#ffffff",	// White
			'1'	: "#FDD017",	// Yellow
			'2'	: "#2B65EC",	// Blue
			'3'	: "#F62817",	// Red
			'4'	: "#7A5DC7",	// Purple
			'5'	: "#F87217",	// Orange
			'6'	: "#348017",	// Green
			'7'	: "#A52A2A",	// Brown
			'8'	: "#000000",	// Black
		}; var clr = colours[ (this.number > 8 ? this.number - 8 : this.number )];
		
		var material = new THREE.MeshPhongMaterial( {color: 0xffffff} ); //default black texture
		material.map = THREEx.createPoolBall.ballTexture( (this.number > 0 ? "" + this.number : ""), this.number>8, clr, 512 );
		
		var position; // pseudo"random" positions
		switch(this.number) {
			case 1:  position = new THREE.Vector3(   58.5, ballRadius, 0 			); break;
			case 2:  position = new THREE.Vector3( 78.316, ballRadius, 11.44 		); break;
			case 3:  position = new THREE.Vector3( 73.362, ballRadius, - ballRadius ); break;
			case 4:  position = new THREE.Vector3( 63.454, ballRadius, ballRadius 	); break;
			case 5:  position = new THREE.Vector3( 78.316, ballRadius, 5.72 		); break;
			case 6:  position = new THREE.Vector3( 68.408, ballRadius, -5.72 		); break;
			case 7:  position = new THREE.Vector3( 78.316, ballRadius, -5.72 		); break;
			case 8:  position = new THREE.Vector3( 68.408, ballRadius, 0 			); break;
			case 9:  position = new THREE.Vector3( 68.408, ballRadius, 5.72 		); break;
			case 10: position = new THREE.Vector3( 73.362, ballRadius, ballRadius 	); break;
			case 11: position = new THREE.Vector3( 78.316, ballRadius, -11.44 		); break;
			case 12: position = new THREE.Vector3( 73.362, ballRadius, -8.58 		); break;
			case 13: position = new THREE.Vector3( 63.454, ballRadius, - ballRadius ); break;
			case 14: position = new THREE.Vector3( 78.316, ballRadius, 0 			); break;
			case 15: position = new THREE.Vector3( 73.362, ballRadius, 8.58 		); break;
			case 0: default: position = new THREE.Vector3(-58.5, ballRadius, 0		); break;
		}
	
		var mesh = new THREE.Mesh( sphere, material );
		mesh.position.copy(position);
		mesh.rotateY( - Math.PI / 2 );
	
		this.ball = mesh;
		
		return this;
	}

	setSpeed (newSpeed){ this.speed.copy(newSpeed); }
	getSpeed () { return this.speed; }
	isRolling() { return this.speed.x != 0.0 || this.speed.y != 0.0; }
	
	getDividedSpeed ( /* int */ divideBy ) {
		var returnVector = new THREE.Vector2().copy( this.speed );
		returnVector.x /=  divideBy;
		returnVector.y /=  divideBy;
		debug( "React V(x:" + this.speed.x + "|y:" + this.speed.y + ") to " + divideBy + " balls => V(x:" + returnVector.x + "|y:" + returnVector.y + ")", 8);
		return returnVector;
	}
	
	reactToVector ( incomingVector ) {
		var minspeed = 1.0;
		if( incomingVector.x <= minspeed && incomingVector.y <= minspeed && incomingVector.x >= -minspeed && incomingVector.y >= -minspeed ){
			incomingVector.x = 0.0;
			incomingVector.y = 0.0;
		}
		
		//TODO is this correct?
		this.speed.x = (this.speed.x + incomingVector.x) / 2;
		this.speed.y = (this.speed.y + incomingVector.y) / 2;
		debug("[COLL] Ball " + this.number + " was hit by V(x:" + incomingVector.x + "|y:" + incomingVector.y + ") => result V(x:" + this.getSpeed().x + "|y:" + this.getSpeed().y +")", 8);
	}
	
	move ( /* float */  friction ){ //The friction that will slow down the ball.
		//HOLE COLLISIONS
		var xs = ([0.0, -116.0, 116.0, 0.0, -116.0, 116.0]);
		var zs = ([59.5, 57.5, 57.5, -59.5, -57.5, -57.5]);
		
		var isHole = false;
		if( Math.abs(this.speed.x) <= 50.0 && Math.abs(this.speed.y) <= 50.0 ) { //maximum speed to fall in hole, otherwise bounce off wall.
			for(var i=0; i<xs.length; i++){
				if ( this.isTouching( new THREE.Vector3(xs[i], 2.86, zs[i])) ) { //is in hole
					
					this.setSpeed( new THREE.Vector2( 0.0, 0.0) ); //reset ball speed
					this.ball.visible = false; //hide it until all balls have stopped rolling, and make it visible with the cue again.
					isHole = true;
					debug("[HOLE] Ball " + this.number + " is at (x:"+this.ball.position.x + "|z:"+this.ball.position.z + ").", 2);
					
					if( this.number == 0 ) { //someone pocketed the white ball
						this.ball.position.x = -58.5;
						this.ball.position.z = 100;
					} else {
						handlePoolEvent( new PoolEvent( "pocket", this.number ) );
					}
					break;
				}
			}
		}

		//WALL COLLISIONS
		var maxX = 117.0 - 2.86, maxZ = 58.5 - 2.86; //max X/Z - .5 * ball.radius
		if(!isHole && (Math.abs( this.ball.position.x ) >= maxX || Math.abs( this.ball.position.z ) >= maxZ) ){
			var dx = this.speed.x / 100.0;
			var dz = this.speed.y / 100.0;
			
			debug("[WALL] Ball " + this.number + " is at (x:"+ this.ball.position.x + "|z:"+ this.ball.position.z + ").", 3);
			if(this.ball.position.x >= maxX) {
				this.ball.position.x = this.ball.position.x - (this.ball.position.x - maxX);
				this.speed.x = -1.0 * this.speed.x;
			} else if(this.ball.position.x <= -maxX){
				this.ball.position.x = this.ball.position.x - (this.ball.position.x + maxX);
				this.speed.x = -1.0 * this.speed.x;
			}
			
			if(this.ball.position.z >= maxZ){
				this.ball.position.z = this.ball.position.z - (this.ball.position.z - maxZ);
				this.speed.y = -1.0 * this.speed.y;
			} else if(this.ball.position.z <= -maxZ){
				this.ball.position.z = this.ball.position.z - (this.ball.position.z + maxZ);
				this.speed.y = -1.0 * this.speed.y;
			}
			
			//this.speed = new THREE.Vector2(0.0, 0.0); //TODO remove DEBUG stop at hitting wall
		}
		
		//UPDATE POSITION AND SPEED
		this.ball.position.x += this.speed.x / 100.0;
		this.ball.position.z += this.speed.y / 100.0;

		this.speed.x = Math.floor( 1000* this.speed.x * friction ) / 1000; //floor speed to 3 decimals ??
		this.speed.y = Math.floor( 1000* this.speed.y * friction ) / 1000; //floor speed to 3 decimals ??
		debug("Ball " + this.number + " has moved " + this.speed.multiplyScalar(1.0/friction).length()/100.0 + " cm with friction " + friction + " and speed V(x:" + this.speed.x + "|y:" + this.speed.y + ").", 8);

		if( Math.abs(this.speed.x) <= 1.0 && Math.abs(this.speed.y) <= 1.0 ) {
			this.speed = new THREE.Vector2(0.0, 0.0);
			debug("[STOP] Ball " + this.number + " has come to a complete stop.", 3);
		} //If the ball slows down enough, stop it completely.
	}
	
	/**
	 * calculate distance between this.center and otherPos
	 * return distance <= 5.72 (twice the ballradius)
	 * @param otherPos THREE.Vector3() from which the X and Z are used, Y is ignored.
	 */
	isTouching ( /* PoolBall */ otherPos ) {
		var deltaX, deltaZ;
		if( typeof otherPos.ball !== "undefined" ) { // otherPos is a ball
			deltaX = Math.abs( this.ball.position.x - otherPos.ball.position.x );
			deltaZ = Math.abs( this.ball.position.x - otherPos.ball.position.z );
		} else { // otherPos is a ball's position. OR HOLE point
			deltaX = Math.abs( this.ball.position.x - otherPos.x );
			deltaZ = Math.abs( this.ball.position.z - otherPos.z );
		}
		var answer = Math.sqrt( (deltaX*deltaX) + (deltaZ*deltaZ) );
		if(false && answer <= 5.72 ) { 
			debug("Touching answer: "+ answer, 6);
		}
		return answer <= 5.72;
	}
	
	toString () {
		var colours	= { '0':"White", '1':"Yellow", '2':"Blue", '3':"Red", '4':"Purple", '5':"Orange", '6':"Green", '7':"Brown", '8':"Black" };
			
		var info = "\nBall " + this.number + ": " + colours[ (this.number > 8 ? this.number - 8 : this.number )] + " (" + (this.number > 8 ? "striped" : "solid" ) + ")";
		info += "\n      Speed:   V(x:" + this.speed.x + "|y:" + this.speed.y + ")";
		info += "\n      Position: (x:" + this.ball.position.x + "|z:" + this.ball.position.z + ")";
		
		return info;
	}
	
}

