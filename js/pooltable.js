class PoolTable {

	constructor () {
		var pooltable = new THREE.Group();
		
		//Cloth of pooltable.
		var clothMaterial = new THREE.MeshLambertMaterial( { color: 0x074b02 } );
		
		//floor
		var floorBSP = new ThreeBSP(  new THREE.Mesh( new THREE.CubeGeometry( 254, 0.1, 137 ), clothMaterial ) );
	
		//borders
		var borderBSP    = new ThreeBSP( new THREE.Mesh( new THREE.CubeGeometry( 254, 10, 137 ) ) );
		var borderCutBSP = new ThreeBSP( new THREE.Mesh( new THREE.CubeGeometry( 234, 10, 117 ) ) );
		borderBSP = borderBSP.subtract( borderCutBSP);
		
		//holes
		var holeGeo = new THREE.CylinderGeometry( 4, 4, 15, 32 );
		var hole = new THREE.Mesh( holeGeo );
		var xs = ([0, -116, 116, 0, -116, 116]);
		var zs = ([59.5, 57.5, 57.5, -59.5, -57.5, -57.5]);
	
		var i, holeBSP;
		for(i=0; i<6; i++) {
			hole.position.x = xs[i];
			hole.position.z = zs[i];		
			holeBSP = new ThreeBSP( hole );
			 floorBSP =  floorBSP.subtract(holeBSP);
			borderBSP = borderBSP.subtract(holeBSP);
		}
		
		var floor = floorBSP.toMesh( clothMaterial);
		floor.geometry.computeVertexNormals();
		floor.position.y = -0.1;
		pooltable.add(floor);
	
		var border = borderBSP.toMesh( clothMaterial);
		border.geometry.computeVertexNormals();
		border.position.y = 4.9;
		pooltable.add(border);
		
		pooltable.position.y = -10;
		
		var dotGeometry = new THREE.CircleGeometry( 1.5, 32 );
		//white dot, starting point of the apex (front ball)
		
		var footMaterial = new THREE.MeshPhongMaterial( { color: 0xdbdbdb } ); //light gray
		var foot = new THREE.Mesh(dotGeometry, footMaterial);
		foot.rotateX(- Math.PI / 2);
		foot.position.x = 58.5;
		pooltable.add( foot );
	
		//black dot, starting point of the white ball
		var headMaterial = new THREE.MeshPhongMaterial( { color: 0x444444 } ); //dark gray 
		var head = new THREE.Mesh(dotGeometry, headMaterial);
		head.rotateX(- Math.PI / 2);
		head.position.x = -58.5;
		pooltable.add(head);
	
		pooltable.position.y = 0;
		return pooltable;
	}

}