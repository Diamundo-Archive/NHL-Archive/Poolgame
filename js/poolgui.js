var gui = new dat.GUI();

function showHelp () { 
	var hlep = 
		  "<b>Poolgame!</b>\n"
		+ "By Kevin Nauta, (c) 2018\n\n"

		+ "<i>Controls:</i>\n"
		+ "Press A/D to rotate the stick clockwise/counterclockwise.\n"
		+ "Use Ctrl + A/D to speed it up, and Shift + A/D to speed it up more.\n"
		+ "Use Shift + Ctrl + A/D to move the cue a quarter of circle.\n"
		+ "Press W/S to decrease/increase the shooting strength, and press \n"
		+ "Space to shoot.\n"
		+ "Use the left-mouse button to press and drag to rotate the table.\n"
		+ "Scroll to zoom in and out. Use the right-mouse button to press and\n"
		+ "drag to rotate the table before shooting, and to move the table\n"
		+ "laterally when any balls are roling.\n\n"
		
		+ "<i>Rules:</i>\n"
		+ "The goal is to pocket all of your balls, and then pocket the 8-ball.\n"
		+ "If you pocket a ball that doesn't belong to you, then boo.\n"
		+ "If you pocket the 8-ball, the game is over automatically and someone wins.\n"
		+ "By default, the first player shall pocket the striped balls (#'s 9 - 15),\n"
		+ "and the second player will pocket the solid-coloured balls (#'s 1 - 7).\n\n"
		; 
	return hlep; 
} //as a function, so it can be called in the console as well, and produce sensible output there.

var parameters = {
	score: '0 - 0',
	turn: 0,
	player: 'Player 1',
	help: function () {
		alert( showHelp() );
	},
	reset: function () {
//		location.reload(); // F5
		location.reload(true); // Shift + F5
	}
};

var currentScore = gui.add(parameters, 'score').name('Score:').listen();
/* currentScore.onChange( function() {
	var oldValue = "old";
	updateScore(oldValue);
}); /**/
function updateScore(newScore){
	parameters.score = newScore;
}

var currentTurn = gui.add(parameters, 'turn').name('Turn:').listen();
/* currentTurn.onFinishChange( function() {
	var oldValue = 10;
	updateTurn(oldValue);
}); /**/
function updateTurn(newTurn){
	parameters.turn = newTurn;
}

var currentPlayer = gui.add(parameters, 'player').name('Current Player:').listen();
/* currentPlayer.onFinishChange( function() {
	var oldValue = "old";
	updatePlayer(oldValue);
}); /**/
function updatePlayerName(newPlayer){
	parameters.player = 'Player ' + (1+newPlayer);
}

function updatePlayer( player ) {
	parameters.player	= player.name;
	parameters.score	= player.score;
	parameters.turn		= player.turn;
}

gui.add(parameters, 'help') .name('Show Help menu');
gui.add(parameters, 'reset').name('Reset the Game');


/** ==================== **/
/** PLAYER STICK COLOURS **/
/** ==================== **/

/*
var playerColours = gui.addFolder("Player Colours");

var colourParameters = { //TODO presets
	playerOne : '#25d7e3',
	playerTwo : '#00e142'
}
var colourOne = playerColours.addColor(colourParameters, 'playerOne').name('Player One').listen();
var colourTwo = playerColours.addColor(colourParameters, 'playerTwo').name('Player Two').listen();

colourOne.onFinishChange( function(newValue) {
	cue.children[0].material.color = hexToRgb(newValue);
	console.log('Player One changed colour to ');
	console.log(hexToRgb(newValue));
});
colourTwo.onFinishChange( function(newValue) {
	cue.children[0].material.color = hexToRgb(newValue);
	console.log('Player Two changed colour to '); 
	console.log(hexToRgb(newValue));
});

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
} /**/
